package testmod

import "fmt"

type Petrson struct {
	Name     string
	SureName string
	Address  address
}

type address struct {
	Country    string
	City       string
	Street     string
	Home       uint
	PostalCode uint
}

func (p *Petrson) PetrsonInfo() {
	fmt.Print("Имя человека: ", p.Name, "\nФамилия человека: ", p.SureName, "\nАдрес: ", p.Address)
}

func NewPerson(name, surename string, country, city, street string, home, postalcode uint) Petrson {
	p := Petrson{
		Name:     name,
		SureName: surename,
		Address: address{
			Country:    country,
			City:       city,
			Street:     street,
			Home:       home,
			PostalCode: postalcode,
		},
	}
	return p
}
